﻿using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    private static PlayerBehaviour _activeScript = null;

    private void Update()
    {
        var touchCount = Input.touchCount;
        if (touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            var screenPos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2));

            if (touch.position.x < Screen.width / 2 && transform.position.x < screenPos.x
                || touch.position.x > Screen.width / 2 && transform.position.x > screenPos.x)
            {
                UpdatePosition(touch.position);
            }
            else
            {
                UpdatePosition(touch.position, true);
            }
        }
    }

    private void UpdatePosition(Vector3 position, bool isPassive = false)
    {
        var newPosition = Camera.main.ScreenToWorldPoint(position);
        var y = isPassive ? -newPosition.y : newPosition.y;

        transform.position = new Vector3(transform.position.x, y, transform.position.z);
    }
}