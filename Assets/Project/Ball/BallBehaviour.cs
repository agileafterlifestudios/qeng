﻿using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    public float Speed;

    private Vector2 direction;
    private Rigidbody2D _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();

        direction = Random.insideUnitCircle.normalized;
//        _rigidbody.velocity = direction * Speed;
    }

    private void Update()
    {
        _rigidbody.velocity = direction * Speed;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var normal = transform.position - other.transform.position;
            normal.Normalize();
            direction = normal;
        }
        else if (other.gameObject.CompareTag("Collider"))
        {
            var normal = other.transform.position - transform.position;
            normal = new Vector3(0, normal.y, 0);
            normal.Normalize();
            direction = Vector2.Reflect(direction, normal);
        }
    }
}